import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

//region FontAwesome Icons

// import '../assets/FontAwesome.Pro.5.15.4.Web/css/fontawesome.min.css'
// import '../assets/FontAwesome.Pro.5.15.4.Web/css/solid.min.css'
// import '../assets/FontAwesome.Pro.5.15.4.Web/css/regular.min.css'
// import '../assets/FontAwesome.Pro.5.15.4.Web/css/light.min.css'
// import '../assets/FontAwesome.Pro.5.15.4.Web/css/brands.min.css'
// import '../assets/FontAwesome.Pro.5.15.4.Web/css/duotone.min.css'

//endregion


Vue.use(Vuetify);

export default new Vuetify({
    theme: { dark: false },
    icons: {
        iconfont: 'mdi',
    },
});
